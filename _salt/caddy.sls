caddy-derezzone:
  service.running:
    - name: caddy
    - reload: true

/srv/derez.zone:
  file.recurse:
    - source: salt://_source
    - clean: true

/etc/caddy/sites/derez.zone:
  file.managed:
    - watch_in:
      - service: caddy-derezzone
    - require:
      - file: /srv/derez.zone
    - contents: |
        # derez.zone {
        #   import logging
        #   root * /srv/derez.zone
        #   file_server
        # }

        # www.derez.zone {
        #   import logging
        #   redir https://derez.zone{uri} 301
        # }
