#!pyobjects

def record(host, type, value):
    BotoRoute53.present(
        "%s_%s" % (host, type),
        name=host,
        value=value,
        zone="ivyleav.es",
        record_type=type,
        ttl=(60*60),
    )


def alias(target, source, nameserver=None):
    ip4 = salt.dnsutil.A(source, nameserver=nameserver)
    if ip4:
        record(target, "A", ip4)

    ip6 = salt.dnsutil.AAAA(source, nameserver=nameserver)
    if ip6:
        record(target, "AAAA", ip6)

def getips(glob):
    ip4 = [
        ip
        for ips in salt.mine.get(glob, 'ipaddr.four').values()
        for ip in ips
    ]
    ip6 = [
        ip
        for ips in salt.mine.get(glob, 'ipaddr.six').values()
        for ip in ips
    ]
    return ip4, ip6

with BotoRoute53.hosted_zone_present(
    "derez.zone.",
    domain_name="derez.zone.",
    comment="",
):
    sh4, sh6 = getips('statichost-*')
    if sh4:
        record('derez.zone', 'A', sh4)
        record('www.derez.zone', 'A', sh4)
    if sh6:
        record('derez.zone', 'AAAA', sh6)
        record('www.derez.zone', 'AAAA', sh6)
